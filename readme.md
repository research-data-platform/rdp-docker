# Resources for local Drupal development

Markus Suhr, markus.suhr@med.uni-goettingen.de

## Using the reverse-proxy docker-compose.yml
* start the reverse-proxy docker-compose before starting any other service with 'docker-compose up -d'
 * upon start: note the created docker-network 
 * enter the name of the network to other docker-compose.yml files by adding:
 ```
networks:
  default:
    external:
      name: ***network_name_here***
 ```
 * to the end of the .yml file
