#!/usr/bin/env bash
#
# Build Docker images for Drupal based Research Data Platform
#
# Markus Suhr, markus.suhr@med.uni-goettingen.de
# https://gitlab.gwdg.de/msuhr1/rdp-docker
#
docker build -t docker.gitlab.gwdg.de/msuhr1/rdp-docker/base:latest base/
docker build -t docker.gitlab.gwdg.de/msuhr1/rdp-docker/crc1190:dev crc1190-dev/
docker build -t docker.gitlab.gwdg.de/msuhr1/rdp-docker/crc1190:production crc1190-prod/
docker build -t docker.gitlab.gwdg.de/msuhr1/rdp-docker/crc1190:latest crc1190-prod/
docker push docker.gitlab.gwdg.de/msuhr1/rdp-docker/base