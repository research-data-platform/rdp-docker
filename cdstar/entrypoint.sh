#!/bin/bash
#
# Move libraries to expected location
mv lib/ /data/cdstar-demo/

# Start CDSTAR server
#
# Parameters:
# -c <config file>
# -b <hostname>
# -p <port>
# For debug output:
# --debug de.gwdg
java -jar cdstar.jar -c cdstar-demo.yaml run -b cdstar -p 8080